package com.seawaterbt.ssm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name="学生vo")
public class StudentVo {

    @Schema(name="学生姓名")
    private String name;

    @Schema(name="学生年龄")
    private Integer age;

    @Schema(name="学生班级")
    private String classname;
}
