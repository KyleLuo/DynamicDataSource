package com.seawaterbt.ssm.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "老师vo")
public class TeacherVo {

    @Schema(name = "老师姓名")
    private String name;

    @Schema(name = "老师年龄")
    private Integer age;

    @Schema(name = "老师教的学科")
    private String subject;
}
