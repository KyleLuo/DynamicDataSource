package com.seawaterbt.ssm.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.seawaterbt.ssm.entity.Teacher;
import com.seawaterbt.ssm.service.TeacherService;
import com.seawaterbt.ssm.vo.TeacherVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "老师", description = "对老师表CRUD")
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @Operation(method = "添加老师",
            parameters = {
                    @Parameter(name = "teacher")
            })
    @PostMapping("/add")
    public String add(@RequestBody TeacherVo teacher) {
        Teacher tea = new Teacher();
        tea.setName(teacher.getName());
        tea.setAge(teacher.getAge());
        tea.setSubject(teacher.getSubject());
        return teacherService.save(tea) ? "添加成功" : "添加失败";
    }

    @Operation(method = "删除老师")
    @DeleteMapping("/delete/{id}")
    public String delete(@Parameter(name = "老师的主键id") @PathVariable(value = "id") Integer id) {
        return teacherService.removeById(id) ? "删除成功" : "删除失败";
    }

    @Operation(method = "修改老师")
    @PostMapping("/update")
    public String update(@RequestBody Teacher teacher) {
        return teacherService.updateById(teacher) ? "修改成功" : "修改失败";
    }

    @Operation(method = "查询老师")
    @GetMapping("/list")
    public List<Teacher> list() {
        Wrapper<Teacher> wrapper = new QueryWrapper<>();
        return teacherService.list(wrapper);
    }
}
