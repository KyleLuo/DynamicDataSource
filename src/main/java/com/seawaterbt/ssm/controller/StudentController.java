package com.seawaterbt.ssm.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.seawaterbt.ssm.entity.Student;
import com.seawaterbt.ssm.service.StudentService;
import com.seawaterbt.ssm.vo.StudentVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "学生", description = "对学生表CRUD")
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Operation(method = "添加学生")
    @PostMapping("/add")
    public String add(@RequestBody StudentVo student) {
        Student stu = new Student();
        stu.setName(student.getName());
        stu.setAge(student.getAge());
        stu.setClassname(student.getClassname());
        return studentService.save(stu) ? "添加成功" : "添加失败";
    }

    @Operation(method = "删除学生")
    @DeleteMapping("/delete/{id}")
    public String delete(@Parameter(name = "学生的主键id") @PathVariable(value = "id") Integer id) {
        return studentService.removeById(id) ? "删除成功" : "删除失败";
    }

    @Operation(method = "修改学生")
    @PostMapping("/update")
    public String update(@RequestBody Student student) {
        return studentService.updateById(student) ? "修改成功" : "修改失败";
    }

    @Operation(method = "查询学生")
    @GetMapping("/list")
    public List<Student> list() {
        Wrapper<Student> wrapper = new QueryWrapper<>();
        return studentService.list(wrapper);
    }
}
